from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
import csv

def get_data():
    data = []
    target = []
    with open('../dataset/diabetes.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in list(csvReader)[1:]:
            data.append([float(x) for x in row[:-1]])
            target.append(int(row[-1]))
    return data, target


def diabetes_model(nb_neighbors):
    print("Number of neighbors:", nb_neighbors)
    data, target = get_data()
    data_train, data_test, target_train, target_test = train_test_split(
        data, target, test_size = 0.4, random_state=1
    )
    classifier_knn = KNeighborsClassifier(n_neighbors = nb_neighbors)
    classifier_knn.fit(data_train, target_train)
    target_pred = classifier_knn.predict(data_test)
    # Finding accuracy by comparing actual response values(y_test)with predicted response value(y_pred)
    print("Accuracy:", metrics.accuracy_score(target_test, target_pred))
    # Providing sample data and the model will make prediction out of that data

    sample = data[:10]
    preds = classifier_knn.predict(sample)
    print("Predictions:", preds)


diabetes_model(3)
