from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn import metrics
import csv

def get_data():
    data = []
    target = []
    with open('../dataset/diabetes.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in list(csvReader)[1:]:
            data.append([float(x) for x in row[:-1]])
            target.append(int(row[-1]))
    return data, target


def diabetes_model(size_x, size_y):
    data, target = get_data()
    data_train, data_test, target_train, target_test = train_test_split(
        data, target, test_size = 0.4, random_state=1
    )
    clf = MLPClassifier(solver='lbfgs', alpha=1e-5, max_iter=1000,
                        hidden_layer_sizes=(size_x, size_y), random_state=1)
    clf.fit(data_train, target_train)
    target_pred = clf.predict(data_test)
    # Finding accuracy by comparing actual response values(y_test)with predicted response value(y_pred)
    print("Accuracy:", metrics.accuracy_score(target_test, target_pred))
    # Providing sample data and the model will make prediction out of that data

    sample = data[:10]
    preds = clf.predict(sample)
    print("Predictions:", preds)


diabetes_model(4, 10)
